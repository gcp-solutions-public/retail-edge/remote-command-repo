#!/bin/bash


# TODO: Make this into a CLI-type script
#       -- include upstream user (the bastion/initation machine's expected user for the remote to connect with)
#       -- include upstream port (the bastion/initation machine's expected port for the remote to connect with)
#       -- include upstream IP (the bastion/initation machine's IP address for the remote to connect with)
#       -- include upstream pubkey for user (the bastion/initation machine's user's pubkey for the remote to connect with)



SECRET_KEY_PREFIX="reqssh-"

unset TARGET_UNIQUENAME
unset CREATE_NEW_SSH_KEY
unset OVERRIDE_SECRET_NAME
unset OVERRIDE_USE_SSH_KEY

unset PRIVATE_KEY
unset HOST_PORT
SSH_KEY_HOME="${HOME}/.ssh"
HOST_USER="$(whoami)" # default to this user
HOST_IP="$(curl --silent --show-error ifconfig.me)"
PROJECT_ID=$(gcloud config list --format 'value(core.project)' 2> /dev/null) # ignore errors, lack of variable will end execution

function print_help() {
    echo -e "\nUsage: $(basename $0) [required and optional parameters]"
    echo -e "  -h --help\t\t\tThis help menu"
    echo -e "  -t --target <host>\t\tSpecify a unique machine name in the global "
    echo -e "             \t\t\tinventory of machines (hint, typically the hostname of the machine)"
    echo -e "  -s --secret-name <name>\t(Optional) override GCP Secret to store remote configuration in"
    echo -e "\n  Remote Host Configuration (used to establish reverse tunnel)"
    echo -e "  -u --user <name>\t\t(Optional)Specify a user to have the REMOTE machine to establish a connection with"
    echo -e "             \t\t\t(SSH keys need to be configured for this user). Defaults to current user ($(whoami))"
    echo -e "  -p --port <numb>\t\t(REQUIRED)Specify the port to accept ssh tunnel from remote machine"
    echo -e "  --host-ip <ipaddr>\t\tSpecify the IP the remote machine will use to establish an ssh tunnel. Default is (${HOST_IP})"
    exit 1
}

# create_secret <key> <value|filename> <true|false is-file> <PROJECT_ID>
function create_secret() {
    KEY="$1"
    VALUE="$2"
    FILE="${3-false}"
    PROJECT="${4-$PROJECT_ID}"
    EXISTS=$(gcloud secrets describe "${KEY}" --project "${PROJECT}" 2> /dev/null)
    if [[ $? > 0 ]]; then
        gcloud secrets create ${KEY} --replication-policy="automatic" --project=${PROJECT}
    fi
    EXISTS=$(gcloud secrets -q versions access latest --format="value(name)" --secret="${KEY}" --project "${PROJECT}" 2> /dev/null)
    CREATE=false

    if [[ -z "${EXISTS}" ]]; then
        # Only create a new version IF there is no versions
        CREATE=true
    else
        # Secret Exists, check the contents of the latest against the current
        CURR_VALUE=$(gcloud secrets -q versions access latest --secret="${KEY}" --project "${PROJECT}")
        NEW_VALUE="${VALUE}"
        # If this is a file, get the contents of the file, if not, stick with the string value for comparision
        if [[ "$FILE" == "true" ]]; then
            NEW_VALUE=$(cat ${VALUE})
        fi

        COMPARE=$(diff <( printf '%s\n' "${CURR_VALUE}" ) <( printf '%s\n' "${NEW_VALUE}" ))
        if [[ $? -gt 0 ]]; then
            # there is a difference
            echo "Current secret exists, but values are different. New version to be created"
            echo "${COMPARE}"
            CREATE=true
        fi
    fi

    if [[ ${CREATE} == true ]]; then
        if [[ "$FILE" == "false" ]]; then
            # Standard Input
            echo "Creating String Secret"
            echo ${VALUE} | gcloud secrets versions add ${KEY} --project=${PROJECT} --data-file=-
        else
            # File reference
            echo "Creating File Secret"
            gcloud secrets versions add ${KEY} --project=${PROJECT} --data-file=${VALUE}
        fi
    fi

}



VALID_ARGS=$(getopt -o hu:s:p:t:x --long secret-name:,user:,port:,host-ip:,target:,ssh-key,help -- "$@")
if [[ $? -ne 0 ]]; then
    exit 1;
fi

eval set -- "$VALID_ARGS"
while [ : ]; do
  case "$1" in
    -t | --target)
        TARGET_UNIQUENAME="$2"
        shift 2
        ;;
    -t | --target)
        HOST_USER="$2"
        shift 2
        ;;
    -s | --secret-name)
        echo "Overriding default 'GCP Secret Key Name' with '$2'"
        OVERRIDE_SECRET_NAME="$2"
        shift 2
        ;;
    -u | --user)
        HOST_USER="$2"
        shift 2
        ;;
    -k | --ssh-key)
        echo "Using '$2' key"
        OVERRIDE_USE_SSH_KEY="$2"
        shift 2
        ;;
    -p | --port)
        HOST_PORT="$2"
        shift 2
        ;;
    -h | --help)
      print_help
      exit 1
      ;;
    --) shift;
        break
        ;;
  esac
done

if [[ -z "${PROJECT_ID}" ]]; then
    echo -e "\nERROR: Project ID has not been set within the current gcloud configuration.\n\nPlease set using 'gcloud config set project <project-id>' and retry\n"
    print_help
    exit 1
fi

if [[ -z "${TARGET_UNIQUENAME}" ]]; then
    print_help
    exit 1
fi

if [[ -z "${OVERRIDE_USE_SSH_KEY}" ]]; then
    # Create a KeyPair to use for this session (if needed)
    if [[ -f "${SSH_KEY_HOME}/${TARGET_UNIQUENAME}" ]]; then
        # SSH Key already exists, overwrite or create a new one?
        echo ""
        read -p "An SSH Key already exists, overwrite or create a new one? (y/N): " overwrite_key

        if [[ "${overwrite_key}" =~ ^([yY][eE][sS]|[yY])$ ]]; then
            rm -rf ${HOME}/.ssh/${TARGET_UNIQUENAME}
            ssh-keygen -N '' -o -a 100 -t ed25519 -C ${TARGET_UNIQUENAME} -f ${SSH_KEY_HOME}/${TARGET_UNIQUENAME}  > /dev/null
        else
            echo -e "...Skip creating new key\n"
        fi
    else
        echo "Creating a new keypair at ${HOME}/.ssh/${TARGET_UNIQUENAME}"
        ssh-keygen -N '' -o -a 100 -t ed25519 -C ${TARGET_UNIQUENAME} -f ${SSH_KEY_HOME}/${TARGET_UNIQUENAME} > /dev/null
    fi
    PRIVATE_KEY="${SSH_KEY_HOME}/${TARGET_UNIQUENAME}"
else
    PRIVATE_KEY="${OVERRIDE_USE_SSH_KEY}"
fi # end if supplying different location for keys

PUB_KEY=$(cat "${PRIVATE_KEY}.pub")

SECRET="${SECRET_KEY_PREFIX}${TARGET_UNIQUENAME}"

if [[ ! -z "${OVERRIDE_SECRET_NAME}" ]]; then
    SECRET="${OVERRIDE_SECRET_NAME}"
fi

echo -e "\n       Variables             "
echo -e "===============================\n"
echo -e "Public key: \t$PUB_KEY"
echo -e "Target host: \t${TARGET_UNIQUENAME}"
echo -e "GCP Secret: \t${SECRET}"
echo -e "GCP Project: \t${PROJECT_ID}"
echo -e "\nRemote Machine Connection Configuration"
echo -e "===============================\n"
echo -e "User: \t\t\t${HOST_USER}"
echo -e "IP: \t\t\t${HOST_IP}"
echo -e "Port: \t\t\t${HOST_PORT}"
echo -e "SSH Private File: \t${PRIVATE_KEY}"


echo ""
read -p "Do you want to establish a client-initiated SSH tunnel with these varaibles? (y/N): " proceed

##############################################
##############################################
##############################################
##############################################


if [[ "${proceed}" =~ ^([yY][eE][sS]|[yY])$ ]]; then

    BASE64_PRIVATE_KEY="$(cat "${PRIVATE_KEY}" | base64 -w 0)"

    echo $BASE64_PRIVATE_KEY

    tmpfile=$(mktemp /tmp/remote-ssh.XXXXXX)
    cat << EOF > ${tmpfile}
"upstream_user":"${HOST_USER}"
"upstream_pubkey":"${PUB_KEY}"
"upstream_ip":"${HOST_IP}"
"upstream_port":"${HOST_PORT}"
"upstream_private_key":"${BASE64_PRIVATE_KEY}"
EOF

    cat $tmpfile

    # Create Secret (if needed)
    create_secret "${SECRET}" ${tmpfile} "true" "${PROJECT_ID}"

    # Add public key to bastion user's authorized-key file (allow for remote host to SSH to the bastion)
    echo "${PUB_KEY}" >> "${HOME}/.ssh/authorized_keys"

    if [[ $? > 0 ]]; then
        echo "Something went wrong"
        exit 1
    else
        rm -rf $tmpfile
        echo -e "\nSUCCESS:  Secret created and ready for a reverse tunnel for: ${HOST_USER}@${HOST_IP}"
        echo -e "\nSteps to follow:\n1) Create the 'initiate ssh tunnel' configuration directive in the Remote Confiruation repository\n2) Wait up to 1 minute for connection to be established\n3) Run SSH to remote machine 'ssh -i ${PRIVATE_KEY} abm-admin@localhost -P ${HOST_PORT}'"
        exit 0
    fi
else
    echo -e "No prob, retry with different variables\n"
    exit 0
fi
