# Overview

This is an experimental repository to try and initiate an SSH reverse tunnel (outbound SSH) from a target host to a bastion host

## First test

1. Initiate reverse SSH tunnel from `edge-1` to `bastion`
  * Initiate
    * ansible-pull playbook sets a flag?  (general playbook - if my hostname matches a variable or in list of variables)
1. from `bastion`, poll on port to be opened
1. setup ssh from `bastion` to `edge-1`
1. issue SSH commands
1. change back (step 1) initiate reverse SSH directive
  * ansible-playbook reset field in variables (target host will need to close the reverse SSH connection)
  * from ssh connection, remove the reverse ssh connection (thus terminating all SSH)


## Observations
* Ansible playbook will not run if there are local changes (if remoting in and manually running playbook, don't forget to clear local changes)
* Ansible pull has it's own user and `HOME` and therefore the `gcloud login` is not established for ansible-pull