- name: Starting the SSH reverse tunnel
  debug:
    msg:
    - "---------------------------------"
    - "Starting the SSH Reverse Tunnel"
    - "---------------------------------"

# Create config folder
- name: Create local directory for SSH configuration
  file:
    path: "{{ base_folder }}"
    state: directory
    recurse: True
    owner: root
    group: root
    mode: '0700'

- name: Remove host configuration text file
  file:
    path: "{{ base_folder }}/host_configuration.txt || true"
    state: absent

# gcloud secrets versions access latest --secret="reqssh-edge-1"
- name: Get Google Secret for host (if exists)
  shell: |
    gcloud secrets versions access "latest" --secret="{{ target_machines[unique_name].gcp_secret }}" --project "{{ target_machines[unique_name].gcp_project }}" > {{ base_folder }}/host_configuration.txt
  args:
    executable: /bin/bash
  register: script_out

- name: Debug output of secrets script
  debug:
    msg: "{{ script_out }}"
  tags:
    - never
    - debug

- name: Copy file with owner and permissions
  ansible.builtin.copy:
    src: "convert-secret-to-variables.sh"
    dest: "{{ base_folder }}/convert-secret-to-variables.sh"
    owner: root
    group: root
    mode: '0700'

# FIXME: This is a hardcoded sequence that is used by the  "set upstream variables", not a name match
- name: Copy secret extraction script to proper location
  ansible.builtin.command:
    cmd: "{{ base_folder }}/convert-secret-to-variables.sh {{ base_folder }}/host_configuration.txt"
  register: variables

- name: DEBUG output lines from converstion
  debug:
    msg: "{{ variables.stdout_lines }}"
  tags:
    - never
    - debug

# FIXME: Fix this, this is very bad (order of output on script is important). TODO `commands_to_run`: "get-from-secret.sh variable"
- name: Set upstream variables
  set_fact:
    upstream_user: "{{ variables.stdout_lines[0] }}"
    upstream_ip: "{{ variables.stdout_lines[1] }}"
    upstream_port: "{{ variables.stdout_lines[2] }}"
    upstream_privatekey: "{{ variables.stdout_lines[3] }}"
    upstream_pubkey: "{{ variables.stdout_lines[4] }}"

- name: DEBUG - Initiating SSH Tunnel
  debug:
    msg:
      - "Initiating SSH Tunnel from {{ machine_ip }} to {{ upstream_ip }}"
      - "Upstream Port {{ upstream_port }}"
      - "Upstream User {{ upstream_user }}"
      - "Ustream PubKey [{{ upstream_pubkey }}]"
  tags:
    - never
    - debug

- name: Check if pubkey exists already in file
  ansible.builtin.lineinfile:
    path: "{{ ssh_tunnel_pubkey_file }}"
    line: "{{ upstream_pubkey }}"
    create: true
    owner: root
    group: root
    mode: "0600"
  check_mode: true # module level, does not modify file, but checks if it would have
  register: line_check

- name: Add pubkey to file IF it was not present
  ansible.builtin.lineinfile:
    path: "{{ ssh_tunnel_pubkey_file }}"
    line: "{{ upstream_pubkey }}"
    create: true
    owner: root
    group: root
    mode: "0600"
  when: line_check.changed # if it would have been changed

- name: Create SSH config file for `abm-user` allowing upstream to SSH in
  ansible.builtin.lineinfile:
    path: "{{ ssh_tunnel_abm_admin_known_host }}"
    line: "{{ upstream_pubkey }}"
    create: true # if need to, create (unlikely)

- name: Place upstream provided private-key for SSH
  ansible.builtin.copy:
    dest: "{{ ssh_tunnel_privatekey_file }}"
    content: "{{ upstream_privatekey | b64decode }}"
    owner: root
    group: root
    mode: '0400'

- name: DEBUG - Starting reverse SSH to upstream (RUN THIS IN THE SHELL)
  debug:
    msg:
    - ""
    - "nohup ssh -o ServerAliveInterval='{{ ssh_tunnel_alive_interval }}' -o 'UserKnownHostsFile=/dev/null' -o 'StrictHostKeyChecking=no' -o ServerAliveCountMax='{{ ssh_tunnel_max_alive_count }}' -i {{ ssh_tunnel_privatekey_file }} -N -R {{ upstream_port }}:localhost:22 {{ upstream_user }}@{{ upstream_ip }} > {{ ssh_tunnel_execution_log_file }} 2>&1 &"
    - "echo $! > {{ ssh_tunnel_execution_pid_file }}"
    - ""
  # tags:
  #   - never
  #   - debug

- name: Create SSH current debug log
  ansible.builtin.file:
    path: "{{ ssh_tunnel_execution_log_file }}"
    state: touch
    mode: "0600"
    owner: root
    group: root

- name: "Initiate the reverse tunnel and capture the PID"
  shell: |
    nohup ssh -o ServerAliveInterval='{{ ssh_tunnel_alive_interval }}' -o 'UserKnownHostsFile=/dev/null' -o 'StrictHostKeyChecking=no' -o ServerAliveCountMax='{{ ssh_tunnel_max_alive_count }}' -i {{ ssh_tunnel_privatekey_file }} -N -R {{ upstream_port }}:localhost:22 {{ upstream_user }}@{{ upstream_ip }} > {{ ssh_tunnel_execution_log_file }} 2>&1 &
    echo $! > {{ ssh_tunnel_execution_pid_file }}

- name: "DEBUG - Export PID (debug purposes)"
  shell: |
    cat {{ ssh_tunnel_execution_pid_file }}
  register: file_content
  tags:
    - never
    - debug

- name: "DEBUG - Print the PID to console"
  debug:
    msg: "{{ file_content.stdout }}"
  tags:
    - never
    - debug

- name: Ensure PID file is root-only
  ansible.builtin.file:
    path: "{{ ssh_tunnel_execution_pid_file }}"
    owner: "root"
    group: "root"
    mode: "0600"
    modification_time: preserve
    access_time: preserve
