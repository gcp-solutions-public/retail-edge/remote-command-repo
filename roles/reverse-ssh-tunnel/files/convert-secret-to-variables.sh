#!/bin/bash

private_key=""
pubkey=""
port=""
user=""
ip=""

function set_secret_values() {

    FILE=$1

    while IFS= read -r line; do
        LINE=$(echo "$line" | tr -d '"')
        readarray -d ':' -t arr <<< "$LINE"
        KEY="${arr[0]}"
        VALUE=`echo ${arr[1]} | sed -e 's/^[[:space:]]*//'`

        case "$KEY" in
            *"user"*)
                user=$VALUE;;
            *"private_key"*)
                private_key=$VALUE;;
            *"port"*)
                port=$VALUE;;
            *"ip"*)
                ip=$VALUE;;
            *"pubkey"*)
                pubkey=$VALUE;;
        esac
    done < $FILE

}

set_secret_values $1

echo "${user}"
echo "${ip}"
echo "${port}"
echo "${private_key}"
echo "${pubkey}"
