# Overview

This role/module controls a client-side remote SSH tunnel with an upstream (public) host.

## Dependencies

This role requires the host machine to be provisioned for `ansible-pull` and the following Inventory structure available from the `-i` command line.

```
inventory/
   group_vars/
       all.yaml     # variables common to all playbooks run by ansible-pull. Most varaibles unique to the host machine
    inventory.yaml  # Only host listed in the localhost w/ hostname labeling
```

### Variables Needed

cluster_name - Name of the cluster this machine belongs to
machine_label - Name of this machine (the unique name)
machine_ip - This machine's IP


## Important Terms

**Targeted** or **is_targeted** are machines that have a directive to change desired state. These machines are listed in the `vars/main.yml` file with their configuration
**Active SSH Tunnel** is when a machine has created a reverse tunnel to an outbound waiting machine
**Bastion** or **upstream machine** are machines that are configured to wait for an inbound SSH tunnel that has been initiated from the **targeted** host machine

## Future considerations
* Private IP (as long as routable)
* Dynamically create `vars` from a metadata or digital twin server
* Move configuration BOM into GCP Secret (as JSON or something)